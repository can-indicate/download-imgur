browser.runtime.onMessage.addListener(messageHandler);


let imageLinks;


function messageHandler(req, send, response) {
    console.log(req);
    if (req === "button-pressed") {
        buttonPressed();
    } else if (req === "load-html") {
        imageLinks = [];
        console.log("load-html");
        let imagePlaceholders = document.getElementsByClassName("image-placeholder");
        console.log(imagePlaceholders);
        let txt = ``;
        for (let elem of imagePlaceholders) {
            let im = elem.src;
            imageLinks.push(im);
            let imId = im.split('/').pop().split('.').shift();
            txt += `<div><input type="checkbox" id="${imId}" name="${imId}" checked>
                    <label for="${imId}">${imId}</label></div>`;
        }
        browser.runtime.sendMessage(message={imageCheckboxes: txt});
    }
}


function buttonPressed() {
    browser.runtime.sendMessage(message={images: imageLinks});
}
