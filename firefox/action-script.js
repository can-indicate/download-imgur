const NAMESPACE_DOT = "browser.";
let btn = document.getElementById('pressy');
// let dirEdit = document.getElementById('dir-entry');
// let downloadDir = "";

/* Given n and i, format i into a string
* with 0*[1-9][0-9]*, where the number of digits of
* this string equals the number of digits in n.
* i and n must be positive integers, and i must not
* be greater than n.
*/
function stringFormatNumber(i, n) {
    let zeroNumberStringList = `${n}`.split('');
    let originalNumberStringList = `${i}`.split('');
    for (let j = zeroNumberStringList.length - 1; j >= 0; j--) {
        if (originalNumberStringList.length !== 0) {
            zeroNumberStringList[j] = originalNumberStringList.pop();
        } else {
            zeroNumberStringList[j] = '0';
        }
    }
    return zeroNumberStringList.join('');
}

browser.runtime.onMessage.addListener(message => {
    let i = 1;
    if (typeof message.images !== 'undefined') {
        let images = message.images;
        for (let e of images) {
            // let format = (downloadDir == "")? {url: e}: {url: e, };
            // browser.downloads.download(format);
            let fileName;
            let imId = e.split('/').pop().split('.').shift();
            let checkbox = document.getElementById(`${imId}`);
            if (checkbox.checked) {
                if (images.length === 1) {
                    fileName = `${e.split('/').pop()}`;
                } else {
                    fileName = `image${stringFormatNumber(i, images.length)
                                }-${e.split('/').pop()}`;
                }
                browser.downloads.download({url: e, filename: fileName});
            }
            i++;
        }
    }
    if (typeof message.imageCheckboxes !== 'undefined') {
        let checkboxes = document.getElementById('checkboxes');
        checkboxes.innerHTML = message.imageCheckboxes;
    }
});
// dirEdit.addEventListener('input', ev => downloadDir = ev.target.value);
btn.addEventListener('click', () => {
    /* Sending a message from this script to the content script:
    https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/
    WebExtensions/Modify_a_web_page#Messaging 
    first code example.*/
    let querying = browser.tabs.query({active: true, currentWindow: true});
    querying.then(tabs => browser.tabs.sendMessage(tabs[0].id, "button-pressed", null));
})


function sendMessage(msg, respFunc) {
    let tabsFunc = tabs => browser.tabs.sendMessage(tabs[0].id, msg, respFunc);
    if (NAMESPACE_DOT.includes('browser')) {
        let querying = browser.tabs.query({active: true, currentWindow: true});
        querying.then(tabsFunc);
    } else if (NAMESPACE_DOT.includes('chrome')){
        browser.tabs.query({active: true, currentWindow: true}, tabsFunc);
    }
}


Promise.resolve(sendMessage('load-html', null));
